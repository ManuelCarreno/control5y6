#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>
#include <string.h>
float desvStd(estudiante curso[]){ //con esta funcion podemos sacar la desviacion estandar de los promedios del curso
	float promed=0,dif,dif2,base;desv=0;
 	int cont = 0,a=0;suma;
 	while(curso[a].prom != 0.0){
 		promed+=curso[a].prom;
 		a++;
 		cont++;
 	}
 	if(cont == 0){
 		printf("no se ingresaron notas\n");
 		return 0;
 	}
 	promed = promed/cont;
 	
 
 	a = 0;
 	while (a<cont){
 		dif = curso[a].prom -promed;
 		dif2 = dif*dif;
 		suma += dif2;
 		a++;
 	}
 	base = suma/cont;
 	desv= sqrt(base);
 	return desv;
 
}
float menor(float prom[]){  //con esta funcion recorremos los promedios encontrando el menor
	int i;
	float j=7.0;
	for (i=0;i<24;i++){
		if (prom[i]<j){
			j=prom[i]
		}
	}
	return j;
}
float mayor(float prom[]){ //lo mismo que en la anterior pero para encontrar el promedio mayor
	int i;
	float j=1.0;
	for (i=0;i<24;i++){
		if (prom[i]>j){
			j=prom[i]
			
		}
	}
	return j;
}
void registroCurso(estudiante curso[]){ // esta funcion tendra la tarea de pedir al usuario todas las notas para luego sacar promedios
	int i,prom[24],promedpar;
 	for (i=0;i<24;i++){
 		curso[i].prom=0;
 		printf("para el estudiante %s %s %s:\n",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM);
 		printf("ingrese la nota del proyecto n�1\n");
 		scanf("%f",&curso[i].asig_1.proy1);
 		promedpar=promedpar+(curso[i].asig_1.proy1 * 0.20);
 		printf("ingrese la nota del proyecto n�2\n");
 		scanf("%f",&curso[i].asig_1.proy2);
 		promedpar=promedpar+(curso[i].asig_1.proy2 * 0.20);
 		printf("ingrese la nota del proyecto n�3\n");
 		scanf("%f",&curso[i].asig_1.proy3);
 		promedpar=promedpar+(curso[i].asig_1.proy3 * 0.30);
 		printf("ingrese la nota del control n�1\n");
 		scanf ("%f",&curso[i].asig_1.cont1);
 		promedpar=promedpar+(curso[i].asig_1.cont1 * 0.05);
 		printf("ingrese la nota del control n�2\n");
 		scanf ("%f",&curso[i].asig_1.cont2);
 		promedpar=promedpar+(curso[i].asig_1.cont2 * 0.05);
 		printf("ingrese la nota del control n�3\n");
 		scanf ("%f",&curso[i].asig_1.cont2);
 		promedpar=promedpar+(curso[i].asig_1.cont3 * 0.05);
 		printf("ingrese la nota del control n�4\n");
 		scanf ("%f",&curso[i].asig_1.cont2);
 		promedpar=promedpar+(curso[i].asig_1.cont4 * 0.05);
 		printf("ingrese la nota del control n�5\n");
 		scanf ("%f",&curso[i].asig_1.cont2);
 		promedpar=promedpar+(curso[i].asig_1.cont5 * 0.05);
 		printf("ingrese la nota del control n�6\n");
 		scanf ("%f",&curso[i].asig_1.cont2);
 		promedpar=promedpar+(curso[i].asig_1.cont6 * 0.05);
 		prom[i]=promedpar;
 		}
}
void clasificarEstudiantes(char path[], estudiante curso[]){ /*esta funcion abrira y escribira 2 documentos de textos en los cuales
                                                              se guardaran los alumnos que aprobaran y reprobaron de forma separada*/
	char aprobados[1024]="";
	char reprobados[1024]="";
	FILE *aprob
	FILE *reprob
	aprob=fopen("aprobados.txt","w");
	reprob=fopen("reprobados.txt","w");
	int b=0;
	while (b==0){   //con esta estructura repetitva recorremos a los alumnos para que sean clasificados en alguno de los 2 archivos
 		if (curso[i].prom != 0){
 			break;
 		}
		if (curso[i].prom< 4.0 && curso[i].prom != 0){
 			strcat(reprobados ,curso[i].nombre);
 			strcat(reprobados, " ");
 			strcat(reprobados ,curso[i].apellidoP);
 			strcat(reprobados, " ");
 			strcat(reprobados, curso[i].apellidoM);
 			strcat(reprobados, " ");
 			fprintf(reprob,"%s %2f\n",reprobados,curso[i].prom);
 			strcpy(aprobados,"");
 		}
 		else{
 			strcat(aprobados ,curso[i].nombre);
 			strcat(aprobados, " ");
 			strcat(aprobados ,curso[i].apellidoP);
 			strcat(aprobados, " ");
 			strcat(aprobados, curso[i].apellidoM);
 			strcat(aprobados, " ");
 			fprintf(aprob, "%s %2f\n",aprobados,curso[i].prom);
 			strcpy(reprobados,"");
		}
 	}	
}
void metricasEstudiantes(estudiante curso[]){ /*usando las primeras funciones es posible calcular los mejores y peores promedios junto con las desviacion estandar
												llamandolas desde la funcion metricas*/ 
	float desv=desvStd(curso),promedmenor=menor(curso),promedmayor=mayor(curso);
	printf("\nel menor promedio fue: %2f\n el mayor promedio fue:%2f\n la desviacion estandar es: %3f\n",promedmenor,promedmayor,desv);
}
void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}
int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}
